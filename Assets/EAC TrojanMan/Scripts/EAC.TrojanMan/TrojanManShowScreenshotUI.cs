
namespace EAC.TrojanMan
{

// system namespaces
using System.Text.RegularExpressions;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;
using ScreenshotManager = EAC.Utility.ScreenshotManager;

// ==========================================================================
/// <summary>
/// </summary>

public class TrojanManShowScreenshotUI
    : MonoBehaviour
{
    public TrojanManAppManager appManager;

    // UI can be in these states
    private enum UIStates
    {
        ShowImage,      // showing picture just taken
        EnterEmail      // entering email address to send picture
    }

    // UI elements
    private Image m_imgShowScreenshot;
    private Button m_btnCloseScreenshot;
    private Button m_btnEmail;
    private Button m_btnSendEmail;
    private Image m_panelEmailAddress;
    private InputField m_inputEmailAddress;
    private Image m_panelNoInternet;
    private Image m_panelEmailResult;
    private Text m_textEmailResult;

    // current state of UI and path to last picture taken
    private string m_filePathScreenshot = string.Empty;
    private UIStates m_state = UIStates.ShowImage;
    private Regex m_emailRegex = new Regex(k_matchEmailPattern);
    private Coroutine m_showEmailResultCoro = null;

    // RegEx string for recognizing an email address
    private const string k_matchEmailPattern
        = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    private static readonly Color k_emailFailureColor = new Color(0.914f, 0.745f, 0.38f, 0.392f);
    private static readonly Color k_emailSuccessColor = new Color(1.0f, 1.0f, 1.0f, 0.392f);

    #region MonoBehaviour
    protected void Awake ()
    {
        Assert.IsNotNull(appManager);

        m_imgShowScreenshot = ObjectFinder.GetChildComponent<Image>(
            gameObject, "ImageShowScreenshot");
        Assert.IsNotNull(m_imgShowScreenshot);

        m_btnCloseScreenshot = ObjectFinder.GetChildComponent<Button>(
            gameObject, "ButtonCloseScreenshot");
        Assert.IsNotNull(m_btnCloseScreenshot);

        m_btnEmail = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonEmail");
        Assert.IsNotNull(m_btnEmail);

        m_btnSendEmail = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonSendEmail");
        Assert.IsNotNull(m_btnSendEmail);

        m_panelEmailAddress = ObjectFinder.GetChildComponent<Image>(
            gameObject, "PanelEmailAddress");
        Assert.IsNotNull(m_panelEmailAddress);

        m_inputEmailAddress = ObjectFinder.GetChildComponent<InputField>(
            gameObject, "InputFieldEmailAddress");
        Assert.IsNotNull(m_inputEmailAddress);

        m_panelNoInternet = ObjectFinder.GetChildComponent<Image>(
            gameObject, "PanelNoInternetConnectivity");
        Assert.IsNotNull(m_panelNoInternet);

        m_panelEmailResult = ObjectFinder.GetChildComponent<Image>(gameObject, "PanelEmailResult");
        Assert.IsNotNull(m_panelEmailResult);

        m_textEmailResult = ObjectFinder.GetChildComponent<Text>(gameObject, "TextEmailResult");
        Assert.IsNotNull(m_textEmailResult);

        m_panelEmailResult.gameObject.SetActive(false);
    }

    protected void OnEnable ()
    {
        if (ScreenshotManager.instance != null)
        {
            ScreenshotManager.instance.onScreenshotImage.AddListener(OnScreenshotImage);
            ScreenshotManager.instance.onScreenshotSaved.AddListener(OnScreenshotSaved);
        }
    }

    protected void OnDisable ()
    {
        if (ScreenshotManager.instance != null)
        {
            ScreenshotManager.instance.onScreenshotImage.RemoveListener(OnScreenshotImage);
            ScreenshotManager.instance.onScreenshotSaved.RemoveListener(OnScreenshotSaved);
        }
    }
    #endregion MonoBehaviour

    public void OnClickButtonCloseScreenshot ()
    {
        if (m_state == UIStates.ShowImage)
        {
            appManager.SetAppState(TrojanManAppManager.AppState.Running);
        }
        else if (m_state == UIStates.EnterEmail)
        {
            TransitionState(m_state, UIStates.ShowImage);
        }
    }

    public void OnClickButtonEmail ()
    {
        if (!string.IsNullOrEmpty(m_filePathScreenshot))
        {
            Debug.LogFormat(
                "[TrojanManShowScreenshotUI.OnClickButtonEmail] file '{0}'",
                m_filePathScreenshot);

            TransitionState(m_state, UIStates.EnterEmail);
        }
    }

    public void OnClickButtonSendEmail ()
    {
        Debug.Log("[TrojanManShowScreenshotUI.OnClickButtonSendEmail]");
        bool success = true;

        try
        {
            EMailManager.instance.SendMail(m_inputEmailAddress.text, m_filePathScreenshot);
        }
        catch (System.InvalidOperationException ex)
        {
            Debug.LogException(ex);
            success = false;
        }
        catch (System.Net.Mail.SmtpFailedRecipientsException ex)
        {
            Debug.LogException(ex);
            success = false;
        }
        catch (System.Net.Mail.SmtpFailedRecipientException ex)
        {
            Debug.LogException(ex);
            success = false;
        }
        catch (System.Net.Mail.SmtpException ex)
        {
            Debug.LogException(ex);
            success = false;
        }

        if (success)
        {
            m_textEmailResult.text = string.Format(
                "Email sent to:\n<b>{0}</b>", m_inputEmailAddress.text);
            m_panelEmailAddress.color = k_emailSuccessColor;
        }
        else
        {
            m_textEmailResult.text = string.Format(
                "Error sending email to:\n<b>{0}</b>", m_inputEmailAddress.text);
            m_panelEmailAddress.color = k_emailFailureColor;
        }

        if (m_showEmailResultCoro != null)
        {
            StopCoroutine(m_showEmailResultCoro);
            m_showEmailResultCoro = null;
        }

        m_showEmailResultCoro = StartCoroutine(ShowEmailResultCoro());

        TransitionState(m_state, UIStates.ShowImage);
    }

    public void OnScreenshotImage (Texture2D screenshotTexture)
    {
        // inactive button until after image is saved to a file
        m_btnEmail.interactable = false;
        m_filePathScreenshot = string.Empty;

        // display the just taken picture on the UI
        Sprite screenshotSprite
            = Sprite.Create(
                screenshotTexture,
                new Rect(0.0f, 0.0f, screenshotTexture.width, screenshotTexture.height),
                new Vector2(0.5f, 0.5f));
        screenshotSprite.name = "TrojanMan-Screenshot";

        m_imgShowScreenshot.sprite = screenshotSprite;

        TransitionState(m_state, UIStates.ShowImage);
    }

    public void OnScreenshotSaved (string filePath)
    {
        // active button - the picture is now saved to storage
        m_btnEmail.interactable = true;
        m_filePathScreenshot = filePath;
    }

    public void OnInputEmailAddressValueChanged ()
    {
        // activate the "send" button if the user entered an email address
        bool validEmail = m_emailRegex.IsMatch(m_inputEmailAddress.text);

        if (validEmail)
        {
            m_btnSendEmail.interactable = true;
        }
        else
        {
            m_btnSendEmail.interactable = false;
        }
    }

    private System.Collections.IEnumerator ShowEmailResultCoro ()
    {
        m_panelEmailResult.gameObject.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        m_panelEmailResult.gameObject.SetActive(false);
    }

    private void TransitionState (UIStates prevState, UIStates newState)
    {
        if (newState == UIStates.ShowImage)
        {
            m_btnEmail.gameObject.SetActive(true);

            // disable email address input, send button
            m_panelEmailAddress.gameObject.SetActive(false);
            m_inputEmailAddress.gameObject.SetActive(false);
            m_btnSendEmail.gameObject.SetActive(false);
            m_panelNoInternet.gameObject.SetActive(false);
        }
        else if (newState == UIStates.EnterEmail)
        {
            m_btnEmail.gameObject.SetActive(false);

            // enable email address input, send button
            m_panelEmailAddress.gameObject.SetActive(true);
            m_inputEmailAddress.gameObject.SetActive(true);
            m_inputEmailAddress.text = string.Empty;
            m_btnSendEmail.gameObject.SetActive(true);
            m_btnSendEmail.interactable = false;

            if (Application.internetReachability == NetworkReachability.NotReachable)
                m_panelNoInternet.gameObject.SetActive(true);
        }

        m_state = newState;
    }
}

} // namespace EAC.TrojanMan
