
namespace EAC.TrojanMan
{

// system namespaces
using System.Collections.Generic;
// unity namespaces
using UnityEngine.Events;
using UnityEngine;
// external namespaces
using Vuforia;

// ==========================================================================
/// <summary>
/// Hides/Shows a list of objects when Vuforia tracks a target.
/// </summary>

public class TrojanManTrackableEventHandler
    : MonoBehaviour
    , ITrackableEventHandler
{
    // ----------------------------------------------------------------------
    #region Types

    [System.Serializable]
    public class TrackingStateEvent
        : UnityEvent<bool>
    {
    }

    #endregion Types
    // ----------------------------------------------------------------------

    public TrackingStateEvent onTrackingStateChanged = new TrackingStateEvent();

    [Tooltip("Objects that are enabled/disabled if the target is visible/invisible.")]
    public GameObject[] toggledObjects;

    private TrackableBehaviour m_trackableBehaviour;
    private List<Renderer> m_renderers = new List<Renderer>();

    #region MonoBehaviour
    protected void Start ()
    {
        RegisterSelf();
    }

    protected void OnEnable ()
    {
        RegisterSelf();
    }

    protected void OnDisable ()
    {
        UnRegisterSelf();
    }
    #endregion MonoBehaviour

    #region ITrackableEventHandler
    public void OnTrackableStateChanged (
        TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        Debug.LogFormat(
            "[TrojanManTrackableEventHandler.OnTrackableStateChanged] prev '{0}' new '{1}'",
            previousStatus, newStatus);

        if (newStatus == TrackableBehaviour.Status.DETECTED
            || newStatus == TrackableBehaviour.Status.TRACKED
            || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }
    #endregion ITrackableEventHandler

    // ----------------------------------------------------------------------

    private void OnTrackingFound ()
    {
        Debug.Log("[TrojanManTrackableEventHandler.OnTrackingFound]");

        m_renderers.Clear();

        for (int i = 0, iEnd = toggledObjects.Length; i < iEnd; ++i)
        {
            if (toggledObjects[i] != null)
            {
                toggledObjects[i].GetComponentsInChildren<Renderer>(true, m_renderers);
            }
        }

        for (int i = 0, iEnd = m_renderers.Count; i < iEnd; ++i)
        {
            if (m_renderers[i] != null)
                m_renderers[i].enabled = true;
        }

        EmitTrackingStateChanged(true);
    }

    private void OnTrackingLost ()
    {
        Debug.Log("[TrojanManTrackableEventHandler.OnTrackingLost]");

        EmitTrackingStateChanged(false);

        for (int i = 0, iEnd = m_renderers.Count; i < iEnd; ++i)
        {
            if (m_renderers[i] != null)
                m_renderers[i].enabled = false;
        }
    }

    private void EmitTrackingStateChanged (bool found)
    {
        onTrackingStateChanged.Invoke(found);
    }

    private void RegisterSelf ()
    {
        if (m_trackableBehaviour != null)
            return;

        m_trackableBehaviour = GetComponent<TrackableBehaviour>();

        if (m_trackableBehaviour != null)
        {
            m_trackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    private void UnRegisterSelf ()
    {
        if (m_trackableBehaviour == null)
            return;

        m_trackableBehaviour.UnregisterTrackableEventHandler(this);
        m_trackableBehaviour = null;
    }
}

} // namespace EAC.TrojanMan
