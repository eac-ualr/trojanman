
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;

// ==========================================================================
/// <summary>
/// Implements the logc for buttons on the 'Canvas_SettingsUI'.
/// </summary>

public class TrojanManSettingsUI
    : MonoBehaviour
{
    public TrojanManAppManager appManager;

    public EMailSettings emailSettingsEAC;
    public EMailSettings emailSettingsStudentAffairs;
    public EMailSettings emailSettingsAdmissions;
    public EMailSettings emailSettingsAlumni;
    public EMailSettings emailSettingsOrientation;

    private Button m_btnCloseSettings;
    private Toggle m_toggleEmailEAC;
    private Toggle m_toggleEmailStudentAffairs;
    private Toggle m_toggleEmailAdmissions;
    private Toggle m_toggleEmailAlumni;
    private Toggle m_toggleEmailOrientation;

    #region MonoBehaviour
    protected void Awake ()
    {
        Assert.IsNotNull(appManager);

        m_btnCloseSettings = ObjectFinder.GetChildComponent<Button>(
            gameObject, "ButtonCloseSettings");
        Assert.IsNotNull(m_btnCloseSettings);

        m_toggleEmailEAC = ObjectFinder.GetChildComponent<Toggle>(
            gameObject, "ToggleEmailEAC");
        Assert.IsNotNull(m_toggleEmailEAC);

        m_toggleEmailStudentAffairs = ObjectFinder.GetChildComponent<Toggle>(
            gameObject, "ToggleEmailStudentAffairs");
        Assert.IsNotNull(m_toggleEmailStudentAffairs);

        m_toggleEmailAdmissions = ObjectFinder.GetChildComponent<Toggle>(
            gameObject, "ToggleEmailAdmissions");
        Assert.IsNotNull(m_toggleEmailAdmissions);

        m_toggleEmailAlumni = ObjectFinder.GetChildComponent<Toggle>(
            gameObject, "ToggleEmailAlumni");
        Assert.IsNotNull(m_toggleEmailAlumni);

        m_toggleEmailOrientation = ObjectFinder.GetChildComponent<Toggle>(
            gameObject, "ToggleEmailOrientation");
        Assert.IsNotNull(m_toggleEmailOrientation);
    }
    #endregion MonoBehaviour

    public void OnClickButtonCloseSettings ()
    {
        appManager.SetAppState(TrojanManAppManager.AppState.Running);
    }

    public void OnValueChangedToggleEmailEAC ()
    {
        if (m_toggleEmailEAC.isOn)
        {
            EMailManager.instance.SetEMailSettings(emailSettingsEAC);
        }
    }

    public void OnValueChangedToggleEmailStudentAffairs ()
    {
        if (m_toggleEmailStudentAffairs.isOn)
        {
            EMailManager.instance.SetEMailSettings(emailSettingsStudentAffairs);
        }
    }

    public void OnValueChangedToggleEmailAdmissions ()
    {
        if (m_toggleEmailAdmissions.isOn)
        {
            EMailManager.instance.SetEMailSettings(emailSettingsAdmissions);
        }
    }

    public void OnValueChangedToggleEmailAlumni ()
    {
        if (m_toggleEmailAlumni.isOn)
        {
            EMailManager.instance.SetEMailSettings(emailSettingsAlumni);
        }
    }

    public void OnValueChangedToggleEmailOrientation (bool isOn)
    {
        if (isOn)
        {
            EMailManager.instance.SetEMailSettings(emailSettingsOrientation);
        }
    }
}

} // namespace EAC.TrojanMan
