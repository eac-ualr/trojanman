
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;
// local namespaces
using Poses = EAC.TrojanMan.Constants.Poses;

// Debug utility to trigger the different animations of TrojanMan
public class TrojanManAnimationDebug
    : MonoBehaviour
{
    private TrojanManAnimator m_trojanManAnimator = null;

    #region MonoBehaviour
    protected void Awake ()
    {
        m_trojanManAnimator = GetComponent<TrojanManAnimator>();
        Assert.IsNotNull(m_trojanManAnimator);
    }

    protected void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_trojanManAnimator.SetPose(Poses.idle);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_trojanManAnimator.SetPose(Poses.armFlex);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            m_trojanManAnimator.SetPose(Poses.pointing);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            m_trojanManAnimator.SetPose(Poses.chestPound);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            m_trojanManAnimator.SetPose(Poses.superhero);
        }
        else if (Input.GetKey(KeyCode.Alpha6))
        {
            m_trojanManAnimator.SetPose(Poses.robotBattle);
        }
    }
    #endregion MonoBehaviour
}

} // namespace EAC.TrojanMan
