
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;

// ==========================================================================
/// <summary>
/// Implements logic for buttons on 'Canvas_TutorialUI'.
/// </summary>

public class TrojanManTutorialUI
    : MonoBehaviour
{
    public TrojanManAppManager appManager;

    private Button m_btnCloseTutorial;

    #region MonoBehaviour
    protected void Awake ()
    {
        Assert.IsNotNull(appManager);

        m_btnCloseTutorial = ObjectFinder.GetChildComponent<Button>(
            gameObject, "ButtonCloseTutorial");
        Assert.IsNotNull(m_btnCloseTutorial);
    }
    #endregion MonoBehaviour

    public void OnClickButtonCloseTutorial ()
    {
        appManager.SetAppState(TrojanManAppManager.AppState.Running);
    }
}

} // namespace EAC.TrojanMan
