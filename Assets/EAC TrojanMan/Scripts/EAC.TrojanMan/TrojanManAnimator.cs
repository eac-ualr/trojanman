
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine;
using UnityEngine.Assertions;
// local namespaces
using AnimationParameters = EAC.TrojanMan.Constants.AnimationParameters;
using ObjectFinder = EAC.Utility.ObjectFinder;
using Poses = EAC.TrojanMan.Constants.Poses;


public class TrojanManAnimator
    : MonoBehaviour
{
    private Animator m_trojanManAnimator;
    private Animator m_laserBeamAnimator;

    private AudioSource m_audioSourceBounce;
    private AudioSource m_audioSourceTransform;
    private AudioSource m_audioSourceSwing;
    private AudioSource m_audioSourceHit;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    private void Awake  ()
    {
        m_trojanManAnimator = GetComponent<Animator>();
        Assert.IsNotNull(m_trojanManAnimator);

        m_laserBeamAnimator = ObjectFinder.FindObjectComponent<Animator>("Laser_Beam");
        Assert.IsNotNull(m_laserBeamAnimator);

        m_audioSourceBounce
            = ObjectFinder.GetChildComponent<AudioSource>(gameObject, "Audio Source Bounce");
        Assert.IsNotNull(m_audioSourceBounce);
        m_audioSourceTransform
            = ObjectFinder.GetChildComponent<AudioSource>(gameObject, "Audio Source Transform");
        Assert.IsNotNull(m_audioSourceTransform);
        m_audioSourceSwing
            = ObjectFinder.GetChildComponent<AudioSource>(gameObject, "Audio Source Swing");
        Assert.IsNotNull(m_audioSourceSwing);
        m_audioSourceHit
            = ObjectFinder.GetChildComponent<AudioSource>(gameObject, "Audio Source Hit");
        Assert.IsNotNull(m_audioSourceHit);
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void SetPose (int pose)
    {
        m_trojanManAnimator.SetInteger(AnimationParameters.paramPoses, pose);

        int laserBeamPose = Poses.laserBeamIdle;

        if (pose == Poses.superhero)
        {
            laserBeamPose = Poses.laserBeamSuperhero;
        }
        else if (pose == Poses.robotBattle)
        {
            laserBeamPose = Poses.laserBeamRobotBattle;
        }

        m_laserBeamAnimator.SetInteger(AnimationParameters.paramPoses, laserBeamPose);
    }

    public void AnimEventBounce ()
    {
        m_audioSourceBounce.Play();
    }

    public void AnimEventTransform ()
    {
        m_audioSourceTransform.Play();
    }

    public void AnimEventSwing ()
    {
        m_audioSourceSwing.Play();
    }

    public void AnimEventHit ()
    {
        m_audioSourceHit.Play();
    }
}

} // namespace EAC.TrojanMan
