
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;
// local namespaces
using ScreenshotManager = EAC.Utility.ScreenshotManager;

// ==========================================================================
/// <summary>
/// Manager class that keeps track of the current state of the TrojanMan app.
/// </summary>
/// Currently mainly hides/shows various UI Canvas' depending on what the
/// app is doing, e.g. hides everything but the logos while taking a picture.

public class TrojanManAppManager
    : MonoBehaviour
{
    public enum AppState
    {
        ShowHelp,
        TakePicture,
        ShowPicture,
        Running,
        ShowSettings
    }

    public Canvas canvasPictureUI;
    public Canvas canvasTutorialUI;
    public Canvas canvasShowScreenshotUI;
    public Canvas canvasSettings;
    public Canvas canvasHint;
    public TrojanManTrackableEventHandler trojanManEventHandler;

    private AppState m_appState;

    #region MonoBehaviour
    protected void Awake ()
    {
        Assert.IsNotNull(canvasPictureUI);
        Assert.IsNotNull(canvasTutorialUI);
        Assert.IsNotNull(canvasShowScreenshotUI);
        Assert.IsNotNull(canvasSettings);
        Assert.IsNotNull(canvasHint);
        Assert.IsNotNull(trojanManEventHandler);

        TransitionAppState(AppState.ShowHelp, AppState.ShowHelp);
    }

    protected void OnEnable ()
    {
        if (ScreenshotManager.instance != null)
        {
            ScreenshotManager.instance.onScreenshotPre.AddListener(OnScreenshotPre);
            ScreenshotManager.instance.onScreenshotImage.AddListener(OnScreenshotImage);
        }
    }

    protected void OnDisable ()
    {
        if (ScreenshotManager.instance != null)
        {
            ScreenshotManager.instance.onScreenshotPre.RemoveListener(OnScreenshotPre);
            ScreenshotManager.instance.onScreenshotImage.RemoveListener(OnScreenshotImage);
        }
    }
    #endregion MonoBehaviour

    public void SetAppState (AppState newState)
    {
        if (newState != m_appState)
        {
            TransitionAppState(m_appState, newState);
        }
    }

    public AppState GetAppState ()
    {
        return m_appState;
    }

    // ----------------------------------------------------------------------

    private void OnScreenshotPre ()
    {
        SetAppState(AppState.TakePicture);
    }

    private void OnScreenshotImage (Texture2D screenshotTexture)
    {
        SetAppState(AppState.ShowPicture);
    }

    private void TransitionAppState (AppState prevState, AppState newState)
    {
        DeactivateState(prevState);

        m_appState = newState;

        ActivateState(newState);
    }

    private void ActivateState (AppState newState)
    {
        switch (newState)
        {
        case AppState.ShowHelp:
            canvasPictureUI.enabled = false;
            canvasTutorialUI.enabled = true;
            canvasShowScreenshotUI.enabled = false;
            canvasSettings.enabled = false;
            canvasHint.enabled = false;
            trojanManEventHandler.enabled = false;
            break;

        case AppState.TakePicture:
            canvasPictureUI.enabled = false;
            canvasTutorialUI.enabled = false;
            canvasShowScreenshotUI.enabled = false;
            canvasSettings.enabled = false;
            canvasHint.enabled = false;
            trojanManEventHandler.enabled = true;
            break;

        case AppState.ShowPicture:
            canvasPictureUI.enabled = false;
            canvasTutorialUI.enabled = false;
            canvasShowScreenshotUI.enabled = true;
            canvasSettings.enabled = false;
            canvasHint.enabled = false;
            trojanManEventHandler.enabled = false;
            break;

        case AppState.Running:
            canvasPictureUI.enabled = true;
            canvasTutorialUI.enabled = false;
            canvasShowScreenshotUI.enabled = false;
            canvasSettings.enabled = false;
            canvasHint.enabled = true;
            trojanManEventHandler.enabled = true;
            break;

        case AppState.ShowSettings:
            canvasPictureUI.enabled = false;
            canvasTutorialUI.enabled = false;
            canvasShowScreenshotUI.enabled = false;
            canvasSettings.enabled = true;
            canvasHint.enabled = false;
            trojanManEventHandler.enabled = false;
            break;
        }
    }

    private void DeactivateState (AppState prevState)
    {
        switch (prevState)
        {
        case AppState.ShowHelp:
            break;

        case AppState.TakePicture:
            break;

        case AppState.ShowPicture:
            break;

        case AppState.Running:
            break;

        case AppState.ShowSettings:
            break;
        }
    }
}

} // namespace EAC.TrojanMan
