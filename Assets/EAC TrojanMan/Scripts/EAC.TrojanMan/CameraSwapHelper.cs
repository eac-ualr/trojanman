
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine;
// external namespaces
using Vuforia;

// ==========================================================================
/// <summary>
/// Utility class to swap Vuforia between the rear and front facing cameras
/// of a mobile device.
/// </summary>
public class CameraSwapHelper
{
    public void SwapCamera ()
    {
        CameraDevice camera = CameraDevice.Instance;

        CameraDevice.CameraDirection currentDirection = camera.GetCameraDirection();

        if (currentDirection == CameraDevice.CameraDirection.CAMERA_BACK
            || currentDirection == CameraDevice.CameraDirection.CAMERA_DEFAULT)
        {
            RestartCamera(CameraDevice.CameraDirection.CAMERA_FRONT);
        }
        else
        {
            RestartCamera(CameraDevice.CameraDirection.CAMERA_BACK);
        }
    }

    // ----------------------------------------------------------------------

    private void RestartCamera (CameraDevice.CameraDirection newDirection)
    {
        CameraDevice camera = CameraDevice.Instance;

        // stop current camera
        camera.Stop();
        camera.Deinit();

        // set video background to be mirrored horizontally for front camera
        VuforiaRenderer renderer = VuforiaRenderer.Instance;
        VuforiaRenderer.VideoBGCfgData config = renderer.GetVideoBackgroundConfig();

        if (newDirection == CameraDevice.CameraDirection.CAMERA_FRONT)
        {
            config.reflection = VuforiaRenderer.VideoBackgroundReflection.ON;
        }
        else
        {
            config.reflection = VuforiaRenderer.VideoBackgroundReflection.OFF;
        }

        renderer.SetVideoBackgroundConfig(config);

        // re-enabel camera with new direction
        camera.Init(newDirection);
        camera.Start();
    }
}

} // namespace EAC.TrojanMan
