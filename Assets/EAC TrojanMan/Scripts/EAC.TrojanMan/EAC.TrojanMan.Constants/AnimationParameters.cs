
namespace EAC.TrojanMan.Constants
{

// unity namespaces
using UnityEngine;

public static class AnimationParameters
{
    public static readonly int paramPoses = Animator.StringToHash("Pose");
}

} // namespace EAC.TrojanMan.Constants
