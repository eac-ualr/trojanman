
namespace EAC.TrojanMan.Constants
{

// unity namespaces
using UnityEngine;

public static class Poses
{
    public const int idle = 0;
    public const int armFlex = 1;
    public const int pointing = 2;
    public const int chestPound = 3;
    public const int superhero = 4;
    public const int robotBattle = 5;

    public const int laserBeamIdle = 0;
    public const int laserBeamSuperhero = 1;
    public const int laserBeamRobotBattle = 2;
}

} // namespace EAC.TrojanMan.Constants
