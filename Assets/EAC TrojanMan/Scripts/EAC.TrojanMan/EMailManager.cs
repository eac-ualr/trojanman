
namespace EAC.TrojanMan
{

// system namespaces
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Net;
using System.Security.Cryptography.X509Certificates;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;

// ==========================================================================
/// <summary>
/// Singleton to send EMail from within an application.
/// IMPORTANT: Change the email account information when using this for
/// different applications!
/// </summary>

public class EMailManager
    : EAC.Utility.MonoBehaviourSingleton<EMailManager>
{
    public EMailSettings defaultEmailSettings;

    // private const string k_sendHost = "smtp.gmail.com";
    // private const int k_sendPort = 587;
    // private const string k_accountEmail = "eac.trojanman@gmail.com";
    // private const string k_accountPassword = "TheTrojansAreSendingPics";

    // private const string k_emailSubject = "UA Little Rock - TrojanMan";
    // private const string k_emailFrom = "cpneumann@ualr.edu";
    // private const string k_emailReplyTo = "eacinfo@ualr.edu";
    // private string k_emailBody
    //     = "Hello," + System.Environment.NewLine
    //     + "thank you for using the UA Little Rock \"TrojanMan AR\" application. "
    //     + "Please find your picture attached to this email!" + System.Environment.NewLine
    //     + System.Environment.NewLine
    //     + "Emerging Analytics Center - University of Arkansas at Little Rock"
    //     + System.Environment.NewLine
    //     + "--" + System.Environment.NewLine
    //     + "Emerging Analytics Center" + System.Environment.NewLine
    //     + "University of Arkansas at Little Rock" + System.Environment.NewLine
    //     + "2801 South University Ave" + System.Environment.NewLine
    //     + "EIT Building, 4th Floor" + System.Environment.NewLine
    //     + "Little Rock, AR 72204" + System.Environment.NewLine
    //     + "(501) 569-8140" + System.Environment.NewLine
    //     + "eacinfo@ualr.edu" + System.Environment.NewLine;

    private EMailSettings m_emailSettings;
    private SmtpClient m_mailClient;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected override void Awake ()
    {
        base.Awake();

        Assert.IsNotNull(defaultEmailSettings);
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void SetEMailSettings (EMailSettings emailSettings)
    {
        if (emailSettings == m_emailSettings)
            return;

        m_emailSettings = emailSettings;

        if (m_emailSettings != null)
        {
            m_mailClient = new SmtpClient(m_emailSettings.sendHost, m_emailSettings.sendPort);
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(
                m_emailSettings.accountUser, m_emailSettings.accountPassword);
            m_mailClient.Credentials = (ICredentialsByHost)credentials;
            m_mailClient.EnableSsl = true;
        }
    }

    // Sends email to the addresses in 'toAddresses' (multiple addresses can be separated by ',')
    // with the file 'imageFileName' as an attachment.
    public void SendMail (string toAddresses, string imageFileName)
    {
        if (m_emailSettings == null)
            SetEMailSettings(defaultEmailSettings);

        MailMessage message = new MailMessage();
        message.From = new MailAddress(m_emailSettings.emailFrom);
        message.To.Add(toAddresses);

        message.Subject = m_emailSettings.emailSubject;
        message.Body = m_emailSettings.emailBody;
        message.ReplyTo = new MailAddress(m_emailSettings.emailReplyTo);

        Attachment imageFileAttachment = MakeImageFileAttachment(imageFileName);
        message.Attachments.Add(imageFileAttachment);

        ServicePointManager.ServerCertificateValidationCallback += ValidateServerCertificate;

        m_mailClient.Send(message);

        ServicePointManager.ServerCertificateValidationCallback -= ValidateServerCertificate;

        message.Dispose();
    }

    public void SendMail (string toAddresses)
    {
        if (m_emailSettings == null)
            SetEMailSettings(defaultEmailSettings);

        MailMessage message = new MailMessage();
        message.From = new MailAddress(m_emailSettings.emailFrom);
        message.To.Add(toAddresses);

        message.Subject = m_emailSettings.emailSubject;
        message.Body = m_emailSettings.emailBody;
        message.ReplyTo = new MailAddress(m_emailSettings.emailReplyTo);

        ServicePointManager.ServerCertificateValidationCallback += ValidateServerCertificate;

        m_mailClient.Send(message);

        ServicePointManager.ServerCertificateValidationCallback -= ValidateServerCertificate;

        message.Dispose();
    }

    // ----------------------------------------------------------------------

    private Attachment MakeImageFileAttachment (string fileName)
    {
        FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        ContentType ct = new ContentType(MediaTypeNames.Image.Jpeg);

        Attachment att = new Attachment(fs, ct);
        ContentDisposition disposition = att.ContentDisposition;
        disposition.FileName = "TrojanMan Picture.jpg";

        return att;
    }

    private bool ValidateServerCertificate (
        object sender, X509Certificate certificate, X509Chain chain,
        SslPolicyErrors sslPolicyErrors)
    {
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            Debug.LogWarningFormat(
                "[EMailManager.ValidateServerCertificate] certificate errors: {0}",
                sslPolicyErrors);
        }

        // TODO: this accepts servers with invalid certificates
        return true;
    }
}

} // namespace EAC.TrojanMan
