
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;

// ==========================================================================
/// <summary>
/// Implements the logic for the buttons on 'Canvas_Logos'.
/// Buttons open the respectice website in the system default browser.
/// </summary>

public class TrojanManLogosUI
    : MonoBehaviour
{
    private Button m_btnUALittleRock;
    private Button m_btnEAC;

    private const string m_urlUALittleRock = "http://ualr.edu";
    private const string m_urlEAC = "http://eac.ualr.edu";

    #region MonoBehaviour
    protected void Awake ()
    {
        m_btnUALittleRock = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonUALittleRock");
        Assert.IsNotNull(m_btnUALittleRock);

        m_btnEAC = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonEAC");
        Assert.IsNotNull(m_btnEAC);
    }
    #endregion MonoBehaviour

    public void OnClickButtonUALittleRock ()
    {
        Application.OpenURL(m_urlUALittleRock);
    }

    public void OnClickButtonEAC ()
    {
        Application.OpenURL(m_urlEAC);
    }
}

} // namespace EAC.TrojanMan
