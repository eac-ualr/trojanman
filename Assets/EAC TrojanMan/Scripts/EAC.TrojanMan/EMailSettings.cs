
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine;

// ==========================================================================
// Stores settings for an account to send email from.

[CreateAssetMenuAttribute(
    fileName="EMailSettings",
    menuName="EAC TrojanMan/EMailSettings",
    order=100)]
public class EMailSettings
    : ScriptableObject
{
    [Header("EMail Server and Account")]
    [Tooltip("Address of server to send email from")]
    public string sendHost;
    [Tooltip("Port on server to connect to")]
    public int sendPort;
    [Tooltip("Account name (email) for authentication with server")]
    public string accountUser;
    [Tooltip("Password for authentication with server")]
    public string accountPassword;

    [Header("EMail Contents")]
    [Tooltip("Subject line of sent email")]
    public string emailSubject;
    [Tooltip("'From' header of sent email")]
    public string emailFrom;
    [Tooltip("'Reply To' header of sent email")]
    public string emailReplyTo;
    [Tooltip("Contents of sent email")]
    [TextArea(5, 15)]
    public string emailBody;
}

} // namespace EAC.TrojanMan
