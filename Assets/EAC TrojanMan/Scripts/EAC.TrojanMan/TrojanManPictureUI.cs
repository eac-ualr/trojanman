
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;
using Poses = EAC.TrojanMan.Constants.Poses;
using ScreenshotManager = EAC.Utility.ScreenshotManager;

// ==========================================================================
/// <summary>
/// Implements the logic for the buttons on 'Canvas_PictureUI'.
/// These control which pose TrojanMan is showing, switching front/back camera,
/// and taking a picture.
/// </summary>

public class TrojanManPictureUI
    : MonoBehaviour
{
    public TrojanManAppManager appManager;
    public TrojanManAnimator trojanManAnimator;
    public AudioClip clipTakePicture;

    private Button m_btnTakePicture;
    private Button m_btnFlipCamera;
    private Button m_btnShowHelp;
    private Button m_btnShowSettings;

    private Button m_btnPoseIdle;
    private Button m_btnPoseArmFlex;
    private Button m_btnPosePoint;
    private Button m_btnPoseChestPound;
    private Button m_btnPoseSuperhero;
    private Button m_btnAnimRobotBattle;

    private CameraSwapHelper m_cameraSwap;

    #region MonoBehaviour
    protected void Awake ()
    {
        Assert.IsNotNull(appManager);

        m_btnTakePicture = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonTakePicture");
        Assert.IsNotNull(m_btnTakePicture);

        m_btnFlipCamera = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonFlipCamera");
        Assert.IsNotNull(m_btnFlipCamera);

        m_btnShowHelp = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonShowHelp");
        Assert.IsNotNull(m_btnShowHelp);

        m_btnShowSettings = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonShowSettings");
        Assert.IsNotNull(m_btnShowSettings);

        m_btnPoseIdle = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonPoseIdle");
        Assert.IsNotNull(m_btnPoseIdle);

        m_btnPoseArmFlex = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonPoseArmFlex");
        Assert.IsNotNull(m_btnPoseArmFlex);

        m_btnPosePoint = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonPosePoint");
        Assert.IsNotNull(m_btnPosePoint);

        m_btnPoseChestPound
            = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonPoseChestPound");
        Assert.IsNotNull(m_btnPoseChestPound);

        m_btnPoseSuperhero
            = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonPoseSuperhero");
        Assert.IsNotNull(m_btnPoseSuperhero);

        m_btnAnimRobotBattle
            = ObjectFinder.GetChildComponent<Button>(gameObject, "ButtonAnimRobotBattle");
        Assert.IsNotNull(m_btnAnimRobotBattle);

        m_cameraSwap = new CameraSwapHelper();
    }

    protected void Start ()
    {
        ActivatePoseIdle();
    }

    protected void OnEnable ()
    {
        if (ScreenshotManager.instance != null)
            ScreenshotManager.instance.onScreenshotSaved.AddListener(OnScreenshotSaved);
    }

    protected void OnDisable ()
    {
        if (ScreenshotManager.instance != null)
            ScreenshotManager.instance.onScreenshotSaved.RemoveListener(OnScreenshotSaved);
    }
    #endregion MonoBehaviour

    public void SetVisible (bool visible)
    {
        gameObject.SetActive(visible);
    }

    public void OnClickButtonTakePicture ()
    {
        AudioSource.PlayClipAtPoint(clipTakePicture, Vector3.zero);
        ScreenshotManager.instance.SaveScreenshot("TrojanMan", "TrojanMan Pictures");
    }

    public void OnClickButtonFlipCamera ()
    {
        m_cameraSwap.SwapCamera();
    }

    public void OnClickButtonShowHelp ()
    {
        appManager.SetAppState(TrojanManAppManager.AppState.ShowHelp);
    }

    public void OnClickButtonShowSettings ()
    {
        appManager.SetAppState(TrojanManAppManager.AppState.ShowSettings);
    }

    public void OnClickButtonPoseIdle ()
    {
        ActivatePoseIdle();
    }

    public void OnClickButtonPoseArmFlex ()
    {
        ActivatePoseArmFlex();
    }

    public void OnClickButtonPosePoint ()
    {
        ActivatePosePoint();
    }

    public void OnClickButtonPoseChestPound ()
    {
        ActivatePoseChestPound();
    }

    public void OnClickButtonPoseSuperhero ()
    {
        ActivatePoseSuperhero();
    }

    public void OnClickButtonAnimRobotBattle ()
    {
        ActivateAnimationRobotBattle();
    }

    public void OnScreenshotSaved (string filePath)
    {
        Debug.LogFormat("Saved screenshot: '{0}'", filePath);
    }

    // ----------------------------------------------------------------------

    private void ActivatePoseIdle ()
    {
        trojanManAnimator.SetPose(Poses.idle);
        m_btnPoseIdle.interactable = false;
        m_btnPoseArmFlex.interactable = true;
        m_btnPosePoint.interactable = true;
        m_btnPoseChestPound.interactable = true;
        m_btnPoseSuperhero.interactable = true;
        m_btnAnimRobotBattle.interactable = true;
    }

    private void ActivatePoseArmFlex ()
    {
        trojanManAnimator.SetPose(Poses.armFlex);
        m_btnPoseIdle.interactable = true;
        m_btnPoseArmFlex.interactable = false;
        m_btnPosePoint.interactable = true;
        m_btnPoseChestPound.interactable = true;
        m_btnPoseSuperhero.interactable = true;
        m_btnAnimRobotBattle.interactable = false;
    }

    private void ActivatePosePoint ()
    {
        trojanManAnimator.SetPose(Poses.pointing);
        m_btnPoseIdle.interactable = true;
        m_btnPoseArmFlex.interactable = true;
        m_btnPosePoint.interactable = false;
        m_btnPoseChestPound.interactable = true;
        m_btnPoseSuperhero.interactable = true;
        m_btnAnimRobotBattle.interactable = false;
    }

    private void ActivatePoseChestPound ()
    {
        trojanManAnimator.SetPose(Poses.chestPound);
        m_btnPoseIdle.interactable = true;
        m_btnPoseArmFlex.interactable = true;
        m_btnPosePoint.interactable = true;
        m_btnPoseChestPound.interactable = false;
        m_btnPoseSuperhero.interactable = true;
        m_btnAnimRobotBattle.interactable = false;
    }

    private void ActivatePoseSuperhero ()
    {
        trojanManAnimator.SetPose(Poses.superhero);
        m_btnPoseIdle.interactable = true;
        m_btnPoseArmFlex.interactable = true;
        m_btnPosePoint.interactable = true;
        m_btnPoseChestPound.interactable = true;
        m_btnPoseSuperhero.interactable = false;
        m_btnAnimRobotBattle.interactable = false;
    }

    private void ActivateAnimationRobotBattle ()
    {
        trojanManAnimator.SetPose(Poses.robotBattle);
        m_btnPoseIdle.interactable = true;
        m_btnPoseArmFlex.interactable = true;
        m_btnPosePoint.interactable = true;
        m_btnPoseChestPound.interactable = true;
        m_btnPoseSuperhero.interactable = true;
        m_btnAnimRobotBattle.interactable = false;
    }
}

} // namespace EAC.TrojanMan
