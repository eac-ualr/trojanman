
namespace EAC.TrojanMan
{

// unity namespaces
using UnityEngine;

public class TestEmail
    : MonoBehaviour
{
    #region MonoBehaviour
    protected void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EMailManager.instance.SendMail("carsten.p.neumann@gmail.com");
        }
    }
    #endregion MonoBehaviour
}

} // namespace EAC.TrojanMan
