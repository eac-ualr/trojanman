
namespace EAC.Utility
{

// system namespaces
using System.Collections;
using System.Runtime.InteropServices;
using System;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine;

public class ScreenshotManager
    : MonoBehaviourSingleton<ScreenshotManager>
{
    public enum ImageFileType
    {
        PNG,
        JPG
    }

    public enum SaveStatus
    {
        NotSaved,
        Saved,
        Denied,
        Timeout,
    }

    public class ScreenshotEvent
        : UnityEvent
    {
    }

    public class ScreenshotImageEvent
        : UnityEvent<Texture2D>
    {
    }

    public class ScreenshotSaveEvent
        : UnityEvent<string>
    {
    }

    public ScreenshotEvent onScreenshotPre = new ScreenshotEvent();
    public ScreenshotImageEvent onScreenshotImage = new ScreenshotImageEvent();
    public ScreenshotEvent onScreenshotPost = new ScreenshotEvent();
    public ScreenshotSaveEvent onScreenshotSaved = new ScreenshotSaveEvent();

    private const int k_maxSaveRetryCount = 30;

#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern int saveToGallery(string path);

#elif UNITY_ANDROID
    private AndroidJavaClass m_classGalleryScrenshot;
#endif

    // ----------------------------------------------------------------------

    #region MonoBehaviour
    protected override void Awake ()
    {
        base.Awake();

#if UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            m_classGalleryScrenshot
                = new AndroidJavaClass("com.secondfury.galleryscreenshot.MainActivity");
        }
#endif
    }
    #endregion MonoBehaviour

    public void SaveScreenshot (
        string fileName, string albumName, ImageFileType fileType = ImageFileType.JPG,
        Rect captureArea = default(Rect))
    {
        if (captureArea == default(Rect))
        {
            captureArea = new Rect(0.0f, 0.0f, Screen.width, Screen.height);
        }

        StartCoroutine(SaveScreenshotCoro(fileName, albumName, fileType, captureArea));
    }

    // ----------------------------------------------------------------------

    private IEnumerator SaveScreenshotCoro (
        string fileName, string albumName, ImageFileType fileType, Rect captureArea)
    {
        Debug.Log("[ScreenshotManager.SaveScreenshotCoro] >>");

        // notify listeners that this frame a screenshot is taken
        onScreenshotPre.Invoke();

        yield return new WaitForEndOfFrame();
        Texture2D screenshotTexture = TakeScreenshot(captureArea);

        // notify listeners and allow them to modify the texture
        onScreenshotImage.Invoke(screenshotTexture);
        // notify listeners that screenshot was taken
        onScreenshotPost.Invoke();

        byte[] imageData = EncodeScreenshot(screenshotTexture, fileType);

        // release texture - it is no longer needed
        screenshotTexture = null;

        yield return SaveImageCoro (fileName, albumName, fileType, imageData);

        Debug.Log("[ScreenshotManager.SaveScreenshotCoro] <<");
    }

    private Texture2D TakeScreenshot (Rect captureArea)
    {
        Debug.Log("[ScreenshotManager.TakeScreenshot]");

        Texture2D screenshotTexture = new Texture2D(
            (int) captureArea.width, (int) captureArea.height, TextureFormat.RGB24, false);
        const bool updateMipmaps = false;
        const bool makeNonReadable = false;
        screenshotTexture.ReadPixels(captureArea, 0, 0, updateMipmaps);
        screenshotTexture.Apply(updateMipmaps, makeNonReadable);

        return screenshotTexture;
    }

    private byte[] EncodeScreenshot (Texture2D screenshotTexture, ImageFileType fileType)
    {
        Debug.Log("[ScreenshotManager.EncodeScreenshot]");

        byte[] imageData = null;

        switch (fileType)
        {
        case ImageFileType.PNG:
            imageData = screenshotTexture.EncodeToPNG();
            break;

        case ImageFileType.JPG:
            const int jpgQuality = 90;
            imageData = screenshotTexture.EncodeToJPG(jpgQuality);
            break;

        default:
            Assert.IsTrue(false, string.Format("Unhandled ImageFileType '{0}'", fileType));
            break;
        }

        return imageData;
    }

    private IEnumerator SaveImageCoro (
        string fileName, string albumName, ImageFileType fileType, byte[] imageData)
    {
        Debug.Log("[ScreenshotManager.SaveImageCoro]");

        string saveFileName
            = string.Format("{0}_{1}.{2}", fileName,
                System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"), GetFileTypeExt(fileType));

        string saveFilePath = System.IO.Path.Combine(Application.persistentDataPath, saveFileName);

#if UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            saveFilePath = System.IO.Path.Combine(System.IO.Path.Combine(
                Application.persistentDataPath, albumName), saveFileName);

            // make sure directory exists
            string saveDirectory = System.IO.Path.GetDirectoryName(saveFilePath);
            System.IO.Directory.CreateDirectory(saveDirectory);
        }
#endif

        // write image file
        System.IO.File.WriteAllBytes(saveFilePath, imageData);

        // register image with "photo gallery"
        yield return RegisterWithGallery(saveFilePath);

        // notify listeners that image was written
        onScreenshotSaved.Invoke(saveFilePath);
    }

    private IEnumerator RegisterWithGallery (string filePath)
    {
        int retryCount = 0;
        SaveStatus status = SaveStatus.NotSaved;

        while (status == SaveStatus.NotSaved)
        {
            ++retryCount;

            if (retryCount < k_maxSaveRetryCount)
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    status = RegisterWithGalleryAndroid(filePath);
                }
                else if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    status = RegisterWithGalleryIOS(filePath);
                }
                else
                {
                    Debug.LogWarningFormat(
                        "[ScreenshotManager.RegisterWithGallery] Unsupported platform: '{0}'",
                        Application.platform);
                    break;
                }
            }
            else
            {
                status = SaveStatus.Timeout;
            }

            // wait 0.5 seconds and retry
            yield return new WaitForSecondsRealtime(0.5f);
        }
    }

    private SaveStatus RegisterWithGalleryAndroid (string filePath)
    {
#if UNITY_ANDROID
        return (SaveStatus) m_classGalleryScrenshot.CallStatic<int>("addImageToGallery", filePath);
#else
        return SaveStatus.NotSaved;
#endif
    }

    private SaveStatus RegisterWithGalleryIOS (string filePath)
    {
#if UNITY_IOS
        return (SaveStatus) saveToGallery(filePath);
#else
        return SaveStatus.NotSaved;
#endif
    }

    private string GetFileTypeExt (ImageFileType fileType)
    {
        switch (fileType)
        {
        case ImageFileType.PNG:
            return "png";

        case ImageFileType.JPG:
            return "jpg";
        }

        Assert.IsTrue(false);
        return string.Empty;
    }
}

} // namespace EAC.Utility
