
namespace EAC.Utility
{

// unity namespaces
using UnityEngine;

// ==========================================================================
// MonoBehaviourSingleton<T>

public class MonoBehaviourSingleton<T>
    : MonoBehaviour
    where T: MonoBehaviour
{
    public static T instance
    {
        get
        {
            if (m_instance == null)
                SetupInstance();

            return m_instance;
        }
    }

    private static T m_instance = null;
    private static bool m_shutdown = false;

    #region MonoBehaviour
    protected virtual void Awake ()
    {
        m_shutdown = false;
    }

    protected virtual void OnDestroy ()
    {
        Debug.Log("[MonoBehaviourSingleton<T>.OnDestroy]");

        m_shutdown = true;
        m_instance = null;
    }
    #endregion MonoBehaviour

    private static void SetupInstance ()
    {
        if (m_shutdown)
        {
            Debug.LogWarning(
                "[MonoBehaviourSingleton<T>] Attempt to access instance after OnDestroy!");
            return;
        }

        T[] instances = (T[]) FindObjectsOfType(typeof(T));

        if (instances.Length > 0)
        {
            if (instances.Length > 1)
            {
                Debug.LogWarningFormat(
                    "[MonoBehaviourSingleton<T>] Multiple instances of singleton '{0}' found!",
                    typeof(T));

                // destroy all but first instance
                for (int i = 1; i < instances.Length; ++i)
                {
                    Destroy(instances[i]);
                }
            }

            m_instance = instances[0];
        }
        else
        {
            Debug.LogWarningFormat(
                "[MonoBehaviourSingleton<T>] No instance of singleton '{0}' found!",
                typeof(T));

            GameObject singletonGO
                = new GameObject(string.Format("MonoBehaviourSingleton<{0}>", typeof(T)));
            m_instance = singletonGO.AddComponent<T>();
        }
    }
}

} // namespace EAC.Utility
