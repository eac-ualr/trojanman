
namespace EAC.Utility
{

// unity namespaces
using UnityEngine;

// ===========================================================================================
// ObjectFinder
//
// Functions to find GameObjects by name in a specific part of the scene hierarchy.

public class ObjectFinder
{
    // Returns the first child with name 'name' in the scene hierarchy starting at 'root'.
    public static GameObject FindChild (GameObject root, string name)
    {
        FindChildVisitor visitor = new FindChildVisitor(name);
        visitor.Traverse(root);

        return visitor.result;
    }

    // Returns the Component of type 'T' of the first child with name 'name' in the
    // scene hierarchy starting at 'root'.
    public static T GetChildComponent<T> (GameObject root, string name) where T : Component
    {
        GameObject goChild = FindChild(root, name);

        if (goChild != null)
            return goChild.GetComponent<T>();

        return null;
    }

    // Returns the Component of type 'T' on GameObject 'go' or creates one if there is none.
    public static T GetOrCreateComponent<T> (GameObject go) where T : Component
    {
        T component = go.GetComponent<T>();

        if (component == null)
            component = go.AddComponent<T>();

        return component;
    }

    // Returns the Component of type 'T' on the GameObject with name 'name'.
    public static T FindObjectComponent<T> (string name) where T : Component
    {
        GameObject namedGO = GameObject.Find(name);

        if (namedGO != null)
            return namedGO.GetComponent<T>();

        return null;
    }
}

// ===========================================================================================
// FindChildVisitor

internal class FindChildVisitor
    : GameObjectVisitor
{
    private string     m_name;
    public GameObject  result
    {
        get
        {
            return m_result;
        }
    }

    private GameObject m_result;

    public FindChildVisitor (string name)
    {
        m_name   = name;
        m_result = null;
    }

    protected override bool Visit (GameObject go, int depth)
    {
        if(m_result != null)
            return false;

        if(go.name == m_name) {
            m_result = go;
            return false;
        }

        return true;
    }

    protected override void PreTraverse ()
    {
        m_result = null;
    }
}

} // namespace EAC.Utility
