
namespace EAC.Utility
{

// system namespaces
using System.Collections.Generic;
// unity namespaces
using UnityEngine;

// ===========================================================================
// GameObjectVisitor
//
// Helper class to depth first traverse a hierarchy of GameObjects.
// Override the 'Visit' member function to implement functionality
// to be executed when visiting each object.

public abstract class GameObjectVisitor
{
    protected Stack<GameObject> m_Stack;

    public GameObjectVisitor ()
    {
        m_Stack = new Stack<GameObject>();
    }

    public void Traverse (GameObject root, bool visitRoot = true)
    {
        if(root == null)
            return;

        PreTraverse();

        if(visitRoot)
        {
            doTraverse(root, 0);
        }
        else
        {
            m_Stack.Push(root);

            foreach(Transform child in root.transform)
                doTraverse(child.gameObject, 1);

            m_Stack.Pop();
        }

        PostTraverse();
    }

    protected virtual void PreTraverse ()
    {
    }

    // Visit GameObject 'go' which is 'depth' levels below the root.
    // Return 'true' to visit children or 'false' to skip them.
    protected abstract bool Visit (GameObject go, int depth);

    protected virtual void PostTraverse ()
    {
    }

    private void doTraverse (GameObject go, int depth)
    {
        if(go == null)
            return;

        bool visitChildren = Visit(go, depth);

        if(visitChildren)
        {
            m_Stack.Push(go);

            foreach(Transform child in go.transform)
                doTraverse(child.gameObject, depth+1);

            m_Stack.Pop();
        }
    }
}

} // namespace EAC.Utility
